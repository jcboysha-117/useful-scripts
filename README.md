# Useful Scripts

## Active
Scripts I use for things.

### NVD2PwnDoc
This is a simple Python script that takes NVD formatted JSON and turns it into yml files that can be imported to PwnDoc for vulnerability templates based on all of the CVEs based on the NVD data feeds. 

Feeds can be found at https://nvd.nist.gov/vuln/data-feeds

### Randoblog
Because I write a lot in bursts when I have time, and unless I need to publish that day, it goes in the pile for Randoblog to upload. 

## Deprecated
Scripts I used to use for things.

### Autoscan
An automated scanner that outputs a JSON of vulnerabilities on targets in a network. Used for quick and dirty vulnerability assessments or as a recon gathering tool. - This evolved into the [solidScan](https://gitlab.com/jcboysha-117/solidscan) project

