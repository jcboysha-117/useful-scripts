'''
Randoblog! A blog post randomization platform for git* pages. 

Why write posts and publish them immediately?! GO ON YOUR WRITING SPREES!

As long as you use the appropriate markdown, randoblog will copy a random post
to your posts folder and push the repo giving you peace of mind that you 
will get that weekly blog posted. 

Licensed under the GPLv3
(c) 2021 J.C. Boysha

'''

import os
import argparse
import json
import random
import shutil
import datetime
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("--sourceDir", "-sd", help="The directory containing posts"
 + " to be uploaded")
parser.add_argument("--outputDir", "-od", help="The directory where the posts"
 + " go to be uploaded.")
parser.add_argument("--repo", "-re", help="The directory source for the "
 + "git repo")
args = parser.parse_args()

try:
    repo = args.repo
except:
    print("[!] A repository must be specified.")
 
try:
    outDir = args.outputDir
except:
    print("[!] An output directory must be specified.")

try:
    sourceDir = args.sourceDir
except:
    sourceDir = './'

posts = []

for post in (os.listdir(sourceDir)):
    posts.append(post)

chosenOne = random.randint(0,(len(posts)-1))

post = posts[chosenOne]

fileName = sourceDir + "/" + post

if os.path.isfile(fileName):
        fileName2 = (sourceDir + "/" 
                    + datetime.datetime.now().strftime('%Y-%m-%d') + "-" 
                    + post)
        os.rename(fileName, fileName2)
        shutil.move(fileName2, outDir)


subprocess.call(repo + "/commit-push.sh")
