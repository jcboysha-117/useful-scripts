# Randoblog - README

Randoblog is a script that will periodically take a random pre-written blog 
post and post it to my blog. This is for all of the things I feel like writing
on days I want to write 3-4 different blog posts, but posting 3-4 posts a day
isn't really the best way to have a blog... So instead I just drop them in a 
folder Randoblog is pointed to and it posts the blogs.

## How it works
Randoblog has two main features: 
1. It copies a blog post to a jekyll blog directory
2. It kicks off a git push from the application

It also only does this at a configured time. I set mine to the middle of the
night. Speaking of: It has a number of configurable options.

* Source Directory
* Target Directory
* Git repo

So, you save your blogs that you've been bingewriting in a folder ("posts"). 
Randoblog comes in and queries that folder, picks a random file, renames it, 
pushes it to your git* pages blog (Tested on Jekyll), and then uses the 
convenient "commit-push" script to announce to the world that randoblog posted
a blog for you!

## Installation

Installing this script is super simple. Clone the directory, copy the 
commit-push.sh script to your repo, and run the script. 

That's it. That's the whole thing

## Pre-requisites

None at all. All built with pure Python3 stock modules.

## Usage
`python3 randoblog.py --help` will tell you everything you need to know. 

## License

Just like the rest of my Useful-Scripts, this is licensed under the GPLv3