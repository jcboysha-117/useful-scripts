#!/bin/bash

# Commit-push, brought to you by Randoblog! 
# Part of the Randoblog suite
# (C) J.C. Boysha 2021
# Released under the GPLv3

git add _posts/.

git commit -m "Randoblog has given us a post!"

git push
