import json
import yaml, sys
cves = []
try: 
    filename = sys.argv[1]
except: 
    print("N2P converts NVD formatted JSON to PwnDoc formatted YAML to import vulnerabilities.")
    print("Usage: N2P.py <name of Json file>")
    exit(1)

with open('2021.json') as json_f:
    data = json.load(json_f)
    for result in data['CVE_Items']:  
        references = []
        ref_yaml = []
        ref_count = 0

        try:
            cvssv3 = result['impact']['baseMetricV3']['cvssV3']['vectorString']
            cvssScore = result['impact']['baseMetricV3']['cvssV3']['baseScore']
            cvssSeverity = result['impact']['baseMetricV3']['cvssV3']['baseSeverity']
        except: 
            cvssv3 = 'CVSS:3.0/AV:/AC:/PR:/UI:/S:/C:/I:/A:'
            cvssScore = '0'
            cvssSeverity = None
        for reference in result['cve']['references']['reference_data']:
            references.append(reference['url'])
        locale = "en"
        title = result['cve']['CVE_data_meta']['ID'] 
        description = result['cve']['description']['description_data'][0]['value']
        vulnType = "NVD-Import [CVE]" 
        cve = {'cvssv3': cvssv3, 'cvssScore': cvssScore, 'cvssSeverity': cvssSeverity, 'details': {'references':[], 'locale': 'en-us', 'title': title, 'vulnType':"NVD-IMPORT (CVE)", 'description':description}}
        cves.append(cve)
    yaml_file = filename[:-5]+'.yaml'
    print(yaml_file)
    with open(yaml_file, 'w') as yaml_f:
        yaml.dump(cves, yaml_f, sort_keys=False)
    with open(yaml_file[:-5]+'.yml', 'w') as yaml_f:
        with open(yaml_file, 'r') as yaml_f2:
            for line in yaml_f2:
                yaml_f.write(line.replace('    references:', '  - references:'))

exit(0)

